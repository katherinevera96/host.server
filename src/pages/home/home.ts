import { AudioProvider } from 'ionic-audio';
import { Component, Provider } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Player } from '../../models/Player';
import { SongsProvider } from '../../providers/songs/songs';
import { config } from '../../providers/config';
import { PlaylistProvider } from '../../providers/playlist/playlist';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  player: Player;
  playlist: any[];
  playing: any = false;
  config: any = config;
  songPlay:any;
  currentProgress: any;

  constructor(public navCtrl: NavController, 
    private songsProvider: SongsProvider, 
    private playlistProvider: PlaylistProvider) {

    this.playlistProvider.get().subscribe(
        data => this.playlist = data.json(),
        err => console.log(err),
        () => console.log("terminó")
  
      );
    
    // this.refresh();
    //this.player = new Player(this.playlist[1].title, this.playlist[1].artist, "Knights of Cydonia Single", this.playlist[1].cover, this.playlist[1].src,null);
  }

  // refresh(){
  //   setInterval(() => {         
  //       this.playlistProvider.get().subscribe(
  //       data => this.playlist = data.json(),
  //       err => console.log(err),
  //       () => console.log("terminó")
  
  //     );
  //   }, 20000);
  // }

  playSong(song,idSong,title,artist){
    this.songPlay = idSong;
    let player: any = document.querySelector("#player audio");
    player.src = config.service_location+song.src;
    let title1: any = document.querySelector("#player .title");
    title1.innerHTML = title;
    let artist1 : any = document.querySelector("#player .artistAlbum");
    artist1.innerHTML = artist;
    player.play();
    var vid = document.getElementById("myVideo");
    this.currentProgress=player.currentTime ;
    //console.log(idSong+" el obj "+ JSON.stringify(song));

    //console.log(JSON.stringify(this.playlist)+" esta es la play list");
    let cover:any = document.querySelector("#player img");
    cover.src = config.service_location +song.cover;
  }
  progress(tagAudio){
    this.currentProgress= (tagAudio.currentTime/tagAudio.duration)*100;
  }
  playpause(e){
    let audioPlayer = e.target.parentElement.parentElement.querySelector("audio");
    if(audioPlayer.paused){ 
      audioPlayer.play();
      this.playing = true;
    }else{ 
      audioPlayer.pause();
      this.playing = false;
    }
  }
  newItem: string = "";
  toggleNew: boolean = false;
  
 
  backward(e){

    let position = parseInt(this.songPlay)-1;
    this.playlist.forEach(element => {
      console.log(element.title+ "id "+position);
      if(position==element.idSong){
        let player: any = document.querySelector("#player audio");
        player.src = config.service_location+element.src;
        let title1: any = document.querySelector("#player .title");
        title1.innerHTML = element.title;
        let artist1 : any = document.querySelector("#player .artistAlbum");
        artist1.innerHTML = element.artist;
        player.play();
        var vid = document.getElementById("myVideo");
        this.currentProgress=player.currentTime ;
        //console.log(idSong+" el obj "+ JSON.stringify(song));
    
        //console.log(JSON.stringify(this.playlist)+" esta es la play list");
        let cover:any = document.querySelector("#player img");
        cover.src = config.service_location +element.cover;
        this.songPlay= element.idSong;
      }

        
      
    });


  }
  forward(e){

    let position = parseInt(this.songPlay)+1;
    this.playlist.forEach(element => {
      console.log(element.title+" position"+ position);
      if(position==element.idSong){
        let player: any = document.querySelector("#player audio");
        player.src = config.service_location+element.src;
        let title1: any = document.querySelector("#player .title");
        title1.innerHTML = element.title;
        let artist1 : any = document.querySelector("#player .artistAlbum");
        artist1.innerHTML = element.artist;
        player.play();
        var vid = document.getElementById("myVideo");
        this.currentProgress=player.currentTime ;
        let cover:any = document.querySelector("#player img");
        cover.src = config.service_location +element.cover;
        this.songPlay=element.idSong;
      }
        
      
    });
    
    // let nuevo: any = JSON.parse(data);
    // let song2=data[position];
    // console.log(event.src+" position"+this.playlist+" data "+ data[12]);
    // // let player: any = document.querySelector("#player audio");
    // player.src = config.service_location+song2.src;
    // player.play();

  }



}
