import { Injectable } from '@angular/core';
import { Song } from '../../models/Song';
import { Http } from '@angular/http';
import { config } from '../config';

/*
  Generated class for the SongsProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class PlaylistProvider {

  songs: Song[]; 

  constructor(public http: Http) {
    
  }

  get() {
    return this.http.get(config.service_location + "Songs/getList/");
  }

  getAll() {
    return this.http.get(config.service_location + "Songs/get");
  }

}
